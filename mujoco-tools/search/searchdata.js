var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrsuvwxy~",
  1: "im",
  2: "m",
  3: "cmr",
  4: "acdefgimopqrsuw~",
  5: "abcdefhimnopqswxy",
  6: "lmv",
  7: "df",
  8: "df",
  9: "acdhps",
  10: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Pages"
};


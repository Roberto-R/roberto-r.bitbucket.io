var searchData=
[
  ['removetransition',['RemoveTransition',['../classgambol_1_1GaitGenerator.html#aac4f94b23bdb26c3bf70ba4bf5209f21',1,'gambol::GaitGenerator']]],
  ['robot',['Robot',['../classgambol_1_1Robot.html#ae56c3ae92f552a6f7690b4286ba73c95',1,'gambol::Robot::Robot()'],['../classgambol_1_1Robot.html#a1f32d16111cebf1db14e8b7a6ebb4368',1,'gambol::Robot::Robot(RobotName robot)']]],
  ['robotmodel',['RobotModel',['../classgambol_1_1RobotModel.html#a375f9b2b4bb39f9158d6acd55d598900',1,'gambol::RobotModel']]],
  ['robotstate',['RobotState',['../structgambol_1_1RobotState.html#a096aab2b0ec5fbf36f04dbac89e56401',1,'gambol::RobotState::RobotState()=default'],['../structgambol_1_1RobotState.html#a5069990c43dfe82f0de5bc9d36868e42',1,'gambol::RobotState::RobotState(const VectorXd &amp;_qpos, const VectorXd &amp;_qvel, const VectorXd &amp;_u_ff, const VectorXd &amp;_u_fb)']]]
];

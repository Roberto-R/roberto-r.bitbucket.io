var searchData=
[
  ['nlpformulation',['NlpFormulation',['../classgambol_1_1NlpFormulation.html#a94155eec5171505b75682d9f194c9837',1,'gambol::NlpFormulation']]],
  ['nlptofile',['NLPToFile',['../classgambol_1_1NLPToFile.html#ad7dae6bbbdcab9d75632505fc71cf660',1,'gambol::NLPToFile']]],
  ['nodecost',['NodeCost',['../classgambol_1_1NodeCost.html#a73703d18d4046a60245ebd3ac24d2627',1,'gambol::NodeCost']]],
  ['nodederivativecost',['NodeDerivativeCost',['../classgambol_1_1NodeDerivativeCost.html#a063f8aba478a1cf51e04666c6e27c493',1,'gambol::NodeDerivativeCost']]],
  ['nodesconstraint',['NodesConstraint',['../classgambol_1_1NodesConstraint.html#a2f8bf6f477a3aa69e72341548da8aa13',1,'gambol::NodesConstraint']]],
  ['nodesholder',['NodesHolder',['../structgambol_1_1NodesHolder.html#af7d827fb50197a7c1d5282a06a8c4613',1,'gambol::NodesHolder::NodesHolder()=default'],['../structgambol_1_1NodesHolder.html#a840f36fd1d18eebe9069775044ade7d1',1,'gambol::NodesHolder::NodesHolder(NodesVariables::Ptr joint_pos, NodesVariables::Ptr joint_vel, NodesVariables::Ptr torques, const std::vector&lt; NodesVariables::Ptr &gt; &amp;ee_forces, const std::vector&lt; PhaseDurations::Ptr &gt; &amp;phase_durations, NodeTimes::Ptr nodes_times)']]],
  ['nodesvariables',['NodesVariables',['../classgambol_1_1NodesVariables.html#a0e54b84a47cfa7eaba6d9a0004448d01',1,'gambol::NodesVariables']]],
  ['nodetimes',['NodeTimes',['../classgambol_1_1NodeTimes.html#aa17439edd0af0ff90f870abed9dcf7ed',1,'gambol::NodeTimes::NodeTimes(const VecTimes &amp;times)'],['../classgambol_1_1NodeTimes.html#a9b7abe086ceb05bd007b1d6d2a304f67',1,'gambol::NodeTimes::NodeTimes(double t_total, int n_nodes)']]],
  ['nodevalueinfo',['NodeValueInfo',['../structgambol_1_1NodesVariables_1_1NodeValueInfo.html#ae7002fa7f0644015e27d947fed64d169',1,'gambol::NodesVariables::NodeValueInfo::NodeValueInfo()'],['../structgambol_1_1NodesVariables_1_1NodeValueInfo.html#ad1054e6510d69975fe33fe3181ba49fb',1,'gambol::NodesVariables::NodeValueInfo::NodeValueInfo(int node_id, int node_dim)']]]
];
